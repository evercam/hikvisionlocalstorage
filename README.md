# Hikvision Local Storage Widget

Widget to display Hikvision SD Card contents in Hikvision's default playback player

## Usage

Copy and Paste following code to HTML page's body

```html
<body>
...
    <div evercam="localstorage"></div>
    
    <script type="text/javascript" src="hikvision.local.storage.mini.js"></script>
    <script type="text/javascript">
        // Set Evercam Camera ID
        LocalStorage.options.cameraId = 'evercam-camera-id';
        
        // Using OAuth Token
        LocalStorage.options.token = 'evercam-oauth-token';
        
        // OR //
        // Using api_id and api_key
        LocalStorage.options.api_id = 'evercam-api-id';
        LocalStorage.options.api_key = 'evercam-api-key';
        
        // Finally Load the widget
        LocalStorage.Load();
    </script>
...
</body>
```